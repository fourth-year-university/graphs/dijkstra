import sys


class Graph:
    def __init__(self, file_name):
        self.graph = {}
        self.nodes = []
        with open(file_name) as f:
            for line in f:
                fr, to, dst = map(int, line.split())
                if fr not in self.nodes:
                    self.nodes.append(fr)
                if to not in self.nodes:
                    self.nodes.append(to)
                self.graph[fr][to] = dst

    def get_nodes(self):
        return self.nodes

    def get_edges_from_node(self, node):
        connections = []
        if node in self.graph:
            for another_node in self.graph[node]:
                connections.append(another_node)
        return connections

    def get_distance(self, node1, node2):
        return self.graph[node1][node2]

    def dijkstra(self, start):
        unvisited = self.get_nodes()
        shortest = {}
        previous = {}

        for node in unvisited:
            shortest[node] = sys.maxsize

        shortest[start] = 0

        while len(unvisited) > 0:
            current_min = None
            for node in unvisited:
                if current_min is None:
                    current_min = node
                elif shortest[node] < shortest[current_min]:
                    current_min = node

            neighbours = self.get_edges_from_node(current_min)
            for neighbour in neighbours:
                val = shortest[current_min] + self.get_distance(current_min, neighbour)
                if val < shortest[neighbour]:
                    shortest[neighbour] = val
                    previous[neighbour] = current_min

            unvisited.remove(current_min)

        return previous, shortest